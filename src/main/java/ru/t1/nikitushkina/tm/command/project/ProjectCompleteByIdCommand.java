package ru.t1.nikitushkina.tm.command.project;

import ru.t1.nikitushkina.tm.enumerated.Status;
import ru.t1.nikitushkina.tm.util.TerminalUtil;

public final class ProjectCompleteByIdCommand extends AbstractProjectCommand {

    public static final String NAME = "project-complete-by-id";

    public static final String DESCRIPTION = "Complete project by id.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[COMPLETE PROJECT BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        getProjectService().changeProjectStatusById(id, Status.COMPLETED);
    }

}
