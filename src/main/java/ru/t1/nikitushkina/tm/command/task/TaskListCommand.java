package ru.t1.nikitushkina.tm.command.task;

import ru.t1.nikitushkina.tm.enumerated.Sort;
import ru.t1.nikitushkina.tm.model.Task;
import ru.t1.nikitushkina.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class TaskListCommand extends AbstractTaskCommand {

    public static final String NAME = "task-list";

    public static final String DESCRIPTION = "Display all tasks.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[TASK LIST]");
        System.out.println("ENTER SORT TYPE:");
        System.out.println(Arrays.toString(Sort.values()));
        final String sortType = TerminalUtil.nextLine();
        final Sort sort = Sort.toSort(sortType);
        int index = 1;
        final List<Task> tasks = getTaskService().findAll(sort);
        renderTasks(tasks);
    }

}
