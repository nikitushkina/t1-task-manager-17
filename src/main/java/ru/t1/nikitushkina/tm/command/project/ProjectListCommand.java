package ru.t1.nikitushkina.tm.command.project;

import ru.t1.nikitushkina.tm.enumerated.Sort;
import ru.t1.nikitushkina.tm.model.Project;
import ru.t1.nikitushkina.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class ProjectListCommand extends AbstractProjectCommand {

    public static final String NAME = "project-list";

    public static final String DESCRIPTION = "Display all projects.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT LIST]");
        System.out.println("ENTER SORT TYPE:");
        System.out.println(Arrays.toString(Sort.values()));
        final String sortType = TerminalUtil.nextLine();
        final Sort sort = Sort.toSort(sortType);
        int index = 1;
        final List<Project> projects = getProjectService().findAll(sort);
        for (final Project project : projects) {
            if (project == null) continue;
            System.out.println(index + ". " + project);
            index++;
        }
    }

}
